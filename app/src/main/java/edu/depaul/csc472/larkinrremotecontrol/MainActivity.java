package edu.depaul.csc472.larkinrremotecontrol;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.util.Log;

public class MainActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {

    TextView displayPower;
    TextView displayVolume;
    TextView displayChannel;
    Switch power;
    SeekBar volume;
    Button zero;
    Button one;
    Button two;
    Button three;
    Button four;
    Button five;
    Button six;
    Button seven;
    Button eight;
    Button nine;
    Button chUP;
    Button chDOWN;
    Button abc;
    Button nbc;
    Button cbs;
    int currentChannel = 500;
    String requestedChannel = "";
    ChannelSurfer surfer;
    static private final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        displayPower = (TextView) findViewById(R.id.displayPower);
        displayVolume = (TextView) findViewById(R.id.displayVolume);
        displayChannel = (TextView) findViewById(R.id.displayChannel);

        volume = (SeekBar) findViewById(R.id.volume);

        power = (Switch) findViewById(R.id.power);

        zero = (Button) findViewById(R.id.zero);
        one = (Button) findViewById(R.id.one);
        two = (Button) findViewById(R.id.two);
        three = (Button) findViewById(R.id.three);
        four = (Button) findViewById(R.id.four);
        five = (Button) findViewById(R.id.five);
        six = (Button) findViewById(R.id.six);
        seven = (Button) findViewById(R.id.seven);
        eight = (Button) findViewById(R.id.eight);
        nine = (Button) findViewById(R.id.nine);
        chUP = (Button) findViewById(R.id.chUP);
        chDOWN = (Button) findViewById(R.id.chDOWN);
        abc = (Button) findViewById(R.id.abc);
        nbc = (Button) findViewById(R.id.nbc);
        cbs = (Button) findViewById(R.id.cbs);

        surfer = new ChannelSurfer();

        power.setChecked(true);
        power.setOnCheckedChangeListener(this);

        volume.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b){
                Log.d(TAG, "onProgressChanged");
                displayVolume.setText("Volume: "+i);
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar){
                Log.d(TAG, "onStartTrackingTouch");
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar){
                Log.d(TAG, "onStopTrackingTouch");
            }
        });

        int[] buttonRequests = {
                R.id.zero,
                R.id.one,
                R.id.two,
                R.id.three,
                R.id.four,
                R.id.five,
                R.id.six,
                R.id.seven,
                R.id.eight,
                R.id.nine,
                R.id.chUP,
                R.id.chDOWN,
                R.id.cbs,
                R.id.abc,
                R.id.nbc
        };

        for(int br: buttonRequests){
            View view = findViewById(br);
            view.setOnClickListener(surfer);
        }


    }
    private class ChannelSurfer implements View.OnClickListener{
        public void onClick(View view){
            switch(view.getId()){
                case R.id.chDOWN:
                    Log.d(TAG, "ChannelSurfing chDown request");
                    if(currentChannel > 1){
                        currentChannel--;
                        String temp = Integer.toString(currentChannel);
                        if(temp.length() == 1){
                            displayChannel.setText("Current Channel: 00"+temp);
                            Log.d(TAG, "chDOWN 1st if statement");
                        }
                        if(temp.length() == 2){
                            displayChannel.setText("Current Channel: 0"+temp);
                            Log.d(TAG, "chDOWN 2nd if statement");
                        }
                        if(temp.length()== 3){
                            displayChannel.setText("Current Channel: "+temp);
                            Log.d(TAG, "chDOWN 3rd if statement");
                        }
                    }
                    break;
                case R.id.chUP:
                    Log.d(TAG, "channelSurfing chUP request");
                    if(currentChannel < 999){
                        currentChannel++;
                        String temp = Integer.toString(currentChannel);
                        if(temp.length() == 1){
                            displayChannel.setText("Current Channel: 00"+temp);
                            Log.d(TAG, "chUP 1st if");
                        }
                        if(temp.length() == 2){
                            displayChannel.setText("Current Channel: 0"+temp);
                            Log.d(TAG, "chUP 2nd if");
                        }
                        if(temp.length() == 3){
                            displayChannel.setText("Current Channel: "+temp);
                            Log.d(TAG, "chUP 3rd if");
                        }
                    }
                    break;
                case R.id.abc:
                    Log.d(TAG, "channelSurfing ABC request");
                    displayChannel.setText("Current Channel: 100");
                    currentChannel = 100;
                    break;
                case R.id.nbc:
                    Log.d(TAG, "channelSurfing NBC request");
                    displayChannel.setText("Current Channel: 995");
                    currentChannel = 995;
                    break;
                case R.id.cbs:
                    Log.d(TAG, "channelSurfing CBS request");
                    displayChannel.setText("Current Channel: 005");
                    currentChannel = 5;
                    break;
                default:
                    Log.d(TAG, "channelSurfing default case");
                    requestedChannel(view);
                    break;
            }
        }
    }
    private void requestedChannel(View view){
        Log.d(TAG, "requestedChannel");
        String digit = ((Button) view).getText().toString();
        requestedChannel += digit;

        if(requestedChannel.length() == 3){
            int request =  Integer.parseInt(requestedChannel);
            if((request < 1000) && (request > 0)){
                Log.d(TAG, "requestedChannel is a valid channel");
                displayChannel.setText("Current Channel: " + requestedChannel);
                currentChannel = request;
                requestedChannel = "";
            }
            else{
                Log.d(TAG, "requestedChannel invalid channel");
                requestedChannel = "";
            }
        }


    }

    public void onSwitchClicked(View view){
        Switch sw = (Switch) view;
        Log.d(TAG, "onSwitchClicked "+ sw.getTag()+ " "+ (sw.isChecked() ? "on": "off"));
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked == true){
            Log.d(TAG,"onCheckedChanged, isChecked is true");
            displayPower.setText("Power: ON");
            zero.setClickable(true);
            one.setClickable(true);
            two.setClickable(true);
            three.setClickable(true);
            four.setClickable(true);
            five.setClickable(true);
            six.setClickable(true);
            seven.setClickable(true);
            eight.setClickable(true);
            nine.setClickable(true);
            chUP.setClickable(true);
            chDOWN.setClickable(true);
            abc.setClickable(true);
            nbc.setClickable(true);
            cbs.setClickable(true);
            volume.setEnabled(true);
        }
        else{
            Log.d(TAG, "onCheckedChanged, isChecked is false");
            displayPower.setText("Power: OFF");
            zero.setClickable(false);
            one.setClickable(false);
            two.setClickable(false);
            three.setClickable(false);
            four.setClickable(false);
            five.setClickable(false);
            six.setClickable(false);
            seven.setClickable(false);
            eight.setClickable(false);
            nine.setClickable(false);
            chUP.setClickable(false);
            chDOWN.setClickable(false);
            abc.setClickable(false);
            nbc.setClickable(false);
            cbs.setClickable(false);
            volume.setEnabled(false);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
